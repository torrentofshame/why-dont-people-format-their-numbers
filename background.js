const extension_id = "podmdilnbokjlfjelmomaikopimbfghn";
const placevalue_names = [
    "",
    " thousand",
    " million",
    " billion",
    " trillion",
    " quadrillion",
    " quintrillion",
    " sextillion",
    " septillion",
    " octillion",
    " nonillion",
    " decillion"
];


function fix_number (n) {
    let pretty_num = Number(n).toLocaleString("en-US");
    let pretty_num_array = pretty_num.split(",");
    let prtn_len = pretty_num_array.length;
    let worded_num_array = [];
    for (let i = 0; i < prtn_len; i++) {
        let nstr = ""
        if (pretty_num_array[i].length === 3) {
            let h = pretty_num_array[i].slice(0, 1);
            if (h !== "0") nstr += `${h} hundred`;
            let to = pretty_num_array[i].slice(1);
            if (to.slice(0, 1) === "0" && to !== "00") {
                nstr += " " + to.slice(1);
            } else if (to !== "00") {
                nstr += " " + to;
            }
        } else nstr += pretty_num_array[i];
        if (nstr !== "") {
            nstr += placevalue_names[prtn_len-i-1];
        }
        worded_num_array.push(nstr);
    }
    let worded_num = worded_num_array.filter((s) => s !== "").join(" ");
    return [pretty_num, worded_num];
}


chrome.contextMenus.create({"title": "Illegible Number? I Fix!", "contexts": ["selection"],
    "onclick": (info, _) => {
        let bad_number = info.selectionText;
        let fixed_number = fix_number(bad_number);
        chrome.notifications.create("IllegibleNumberFixer", {
            "type": "basic", "iconUrl": `chrome-extension://${extension_id}/fixingnums_icon.png`,
            "title": "Illegible Number Fixer",
            "message": `That number is: ${fixed_number[0]} or ${fixed_number[1]}`
        });
    }})
